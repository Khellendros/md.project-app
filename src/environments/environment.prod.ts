export const environment = {
  production: true,
  name: 'production',
  loglevel: 'error',
  rootUrl: '/',
  apiEndPoints: {
  	api: 'https://md-project-api.herokuapp.com'
  },
};

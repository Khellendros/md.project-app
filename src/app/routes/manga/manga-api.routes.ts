import { environment } from '../../../environments/environment'

// import { examples_fake } from '../../fake_data/examples/examples'

const manga_api_url: string = environment.apiEndPoints.api

const mangas_api = {
	search: {
		url: manga_api_url + '/api/v1/searches/search',
		method: 'post',
		request_params: ['title'],
		response_params: ['results'], // results = [{cover, title, link}]
	},
	create_user: {
		url: manga_api_url + '/api/v1/users/user',
		method: 'post',
		request_params: ['uuid'],
		response_params: ['uuid', 'error'],
	},
	create_bookmark: {
		url: manga_api_url + '/api/v1/mangas/manga',
		method: 'post',
		request_params: ['link', 'title', 'cover', 'uuid'],
		response_params: ['manga_id', 'bookmark_id'],
	},
	show_manga: {
		url: manga_api_url + '/api/v1/mangas/manga/:id',
		method: 'get',
		request_params: ['id'],
		response_params: ['chapters'] // chapters = [{id, title, link, position}]
	},
	show_chapter: {
		url: manga_api_url + '/api/v1/chapters/chapter/:id',
		method: 'get',
		request_params: ['id'],
		response_params: ['images'] // images = [{link, position}]
	},
	get_bookmarks: {
		url: manga_api_url + '/api/v1/users/user/:uuid/bookmarks',
		method: 'get',
		request_params: ['uuid'],
		response_params: ['bookmarks'] // bookmarks = [{bookmark_id, manga_id, manga_cover, manga_title}]
	},
	delete_bookmark: {
		url: manga_api_url + '/api/v1/users/bookmarks/:id',
		method: 'delete',
		request_params: ['id'],
		response_params: ['bookmark_id, destroy, error']
	},
	get_image64: {
		url: manga_api_url + '/api/v1/images/image/show',
		method: 'post',
		request_params: ['link'],
		response_params: ['image64']
	}
}

export const MangaApiRoutes = {
	...mangas_api,
}
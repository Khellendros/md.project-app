import { RouterModule, Routes } from '@angular/router'

/* Manga */

import { DashboardComponent } from '../../pages/dashboard/dashboard.component'
import { SearchComponent } from '../../pages/search/search.component'
import { BookmarksComponent } from '../../pages/bookmarks/bookmarks.component'
import { MangaComponent } from '../../pages/manga/manga.component'
import { ChapterComponent } from '../../pages/chapter/chapter.component'
export const MangasRoutes: Routes = [
	{
		path: '',
		component: DashboardComponent,
	},	
	{
		path: 'search',
		component: SearchComponent,
	},	
	{
		path: 'bookmarks',
		component: BookmarksComponent,
	},
	{
		path: 'mangas/:id',
		component: MangaComponent,
	},
	{
		path: 'chapters/:id',
		component: ChapterComponent
	}
]
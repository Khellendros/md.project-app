import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { HttpClientModule } from '@angular/common/http'
import { AppRoutes } from './app.routes'
import { NgProgressModule } from '@ngx-progressbar/core'
import { RootComponent } from './core/components/root/root.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md'
import { SlideshowModule } from 'ng-simple-slideshow'

import { Components, Models, Services, Helpers } from './core'
import { Pages } from './pages';

@NgModule({
	entryComponents: [
    ...Components
  ],
  declarations: [
    RootComponent,
    ...Components,
    ...Pages,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(AppRoutes),
    HttpClientModule,
    NgProgressModule.forRoot({
      trickleSpeed: 200,
      min: 20,
      color: '#FF0000'
    }),
    MDBBootstrapModule.forRoot(),
    SlideshowModule,
  ],
  providers: [
  	...Models,
  	...Services,
  ],
  bootstrap: [RootComponent]
})
export class AppModule { }

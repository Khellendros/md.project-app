import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { LoggerService } from '../../services/logger/logger.service'

@Component({
  selector: 'app-manga-item',
  templateUrl: './manga-item.component.html',
  styleUrls: ['./manga-item.component.scss']
})
export class MangaItemComponent implements OnInit {

  @Input() public title: string = ''
  @Input() public cover: string = ''
  @Input() public link: string = ''
  @Input() public manga_id: number

  public create: boolean = true

  @Output() private addManga: EventEmitter<object> = new EventEmitter<object>();

  constructor(
    // private logger: LoggerService,
  ) {
    // logger.setPrefix('Mang-Item-Component')
  }

  public ngOnInit() {
    // this.logger.info(this.manga_id)
    if (this.manga_id == null) {
      this.create = true
    } else {
      this.create = false
      this.link = "/mangas/"+this.manga_id
    }
      
  }

  public addBookmark(manga_link: string, manga_title: string, manga_cover: string) {
    this.addManga.emit({link: manga_link, cover: manga_cover, title: manga_title})
  }

}

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms'

@Component({
  selector: 'app-chapter-select',
  templateUrl: './chapter-select.component.html',
  styleUrls: ['./chapter-select.component.scss']
})
export class ChapterSelectComponent implements OnInit {

  @Input() public current_value: number = null;
  @Input() public values: object[] = [];
  @Input() public label: string = '';

  @Output() private chSelect: EventEmitter<number> = new EventEmitter<number>();

  constructor(

  ) { }

  public ngOnInit() {

  }

  public onSelect(value: any) {
    this.current_value = value

  	this.chSelect.emit(this.current_value)
  }
}

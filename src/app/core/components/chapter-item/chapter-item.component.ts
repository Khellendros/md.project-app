import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-chapter-item',
  templateUrl: './chapter-item.component.html',
  styleUrls: ['./chapter-item.component.scss']
})
export class ChapterItemComponent implements OnInit {

  @Input() public title: string = ''
  @Input() public link: string = ''
  @Input() public chapter_id: number = null
  @Input() public position: number = null

  constructor() { }

  public ngOnInit() {
    this.link = '/chapters/' + this.chapter_id 
  }

}

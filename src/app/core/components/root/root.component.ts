import { Component, OnInit, ViewChild } from '@angular/core';
import { LoggerService } from '../../services/logger/logger.service'
import { ApiService } from '../../services/api/api.service'
import { environment } from '../../../../environments/environment'

import { NavigationStart, NavigationEnd, NavigationCancel, Event, Router } from '@angular/router'
import { NgProgressComponent } from '@ngx-progressbar/core'

import { setUserUUID } from '../../helpers/uuid4-generator.helper'

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.scss']
})
export class RootComponent implements OnInit {

  @ViewChild(NgProgressComponent) progressBar: NgProgressComponent;

  constructor(
  	private logger: LoggerService,
    private router: Router,
    private api: ApiService,
  ) { 
  	logger.setPrefix('Root')
  	logger.warn(`Environment ${environment.name}`)

    router.events.subscribe((event: Event) => {
      this.navigationInterceptors(event)
    })

    logger.info(setUserUUID(api))
  }

  private navigationInterceptors(event: Event) {
    if (event instanceof NavigationStart) {
      this.progressBar.start()
    }
    if (event instanceof NavigationEnd) {
      this.progressBar.complete()
    }
    if (event instanceof NavigationCancel) {
      this.progressBar.complete()
    }   
  }

  ngOnInit() {
  }

}

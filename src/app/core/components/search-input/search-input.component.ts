import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { LoggerService } from '../../services/logger/logger.service'

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent implements OnInit {

  @Output() private search: EventEmitter<string> = new EventEmitter<string>();

  constructor(
  	// private logger: LoggerService,
  ) {
  	// logger.setPrefix('Search Input')
  }

  ngOnInit() {
  }

  public onSubmit(searchValue: string ) { 
  	// this.logger.info(searchValue)
  	this.search.emit(searchValue)
  }

}

import { NavbarComponent } from './components/navbar/navbar.component'
import { NavbarMiniComponent } from './components/navbar-mini/navbar-mini.component'
import { SearchInputComponent } from './components/search-input/search-input.component'
import { PreloaderComponent } from './components/preloader/preloader.component'
import { MangaItemComponent } from './components/manga-item/manga-item.component'
import { ChapterItemComponent } from './components/chapter-item/chapter-item.component'
import { ChapterSelectComponent } from './components/chapter-select/chapter-select.component'

export const Components = [
	NavbarComponent,
	NavbarMiniComponent,
	SearchInputComponent,
	PreloaderComponent,
	MangaItemComponent,
	ChapterItemComponent,
	ChapterSelectComponent,
]

export const Models = [

]

export const Helpers = [

]

import { LoggerService } from './services/logger/logger.service'
import { ApiService } from './services/api/api.service'

export const Services = [
	ApiService,
	LoggerService,
]
import { MangaApiRoutes } from '../../../../routes/manga/manga-api.routes'

export class MangaApi {

	private blank_response: Promise<any> = new Promise((resolve, reject) => { resolve(null)})

	constructor(
		private api: any,
	) { }

	public search(manga_name: string): Promise<any> {
		return this.api.post(MangaApiRoutes.search, {title: manga_name})
	}

	public createUser(uuid: string): Promise<any> {
		return this.api.post(MangaApiRoutes.create_user, {uuid: uuid})
	}

	public createBookmark(link: string, title: string, cover: string, uuid: string): Promise<any> {
		return this.api.post(MangaApiRoutes.create_bookmark, {link: link, title: title, cover: cover, uuid: uuid})
	}

	public showManga(id: number): Promise<any> {
		return this.api.get(MangaApiRoutes.show_manga, {id: id})
	}

	public showChapter(id: number): Promise<any> {
		return this.api.get(MangaApiRoutes.show_chapter, {id: id})
	}

	public getBookmarks(uuid: string): Promise<any> {
		return this.api.get(MangaApiRoutes.get_bookmarks, {uuid: uuid})
	}

	public deleteBookmark(id: number): Promise<any> {
		return this.api.delete(MangaApiRoutes.delete_bookmark, {id: id})
	}

	public getImage64(link: string): Promise<any> {
		return this.api.post(MangaApiRoutes.get_image64, {link: link})
	}
	
}

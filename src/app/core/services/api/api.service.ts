import { Injectable } from '@angular/core';
import { URLSearchParams, ResponseContentType } from '@angular/http';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { LoggerService } from './../../services/logger/logger.service'
import { environment } from '../../../../environments/environment'
import { Observable, Subscriber } from 'rxjs'

// import { ExampleApi } from './example/api.example'
import { MangaApi } from './manga/api.manga'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  // public example: ExampleApi = new ExampleApi(this)
  public manga: MangaApi = new MangaApi(this)

  constructor(
  	private logger: LoggerService,
  	private http: HttpClient,
  ) { 
  	logger.setPrefix('API Service')
  }

  private get(request: object = {}, params: object = {}): Promise<any> {
  	return new Promise((resolve, reject) => {
      if (environment.name != 'development') {
        const data = this.getFakeData(params, request)
        this.logger.info('Fake data', data)
        resolve(data)
      } else {
        this.http.get(this.normalizeUrl(request['url'], params), {
          headers: this.getHeaders(),
        }).subscribe((x) => {
          if (x['error']) {
            reject(x)
          } else {
            resolve(x)
          }
        })
      }
  	})
  }  

  private post(request: object = {}, params: object = {}): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(this.normalizeUrl(request['url'], params), this.getParams(params), {
        headers: this.getHeaders(),
      }).subscribe((x) => {
        if(x['error']) {
          reject(x)
        } else {
          resolve(x)
        }
      })
    })
  }

  private put(request: object = {}, params: object = {}): Promise<any> {
  	return new Promise((resolve, reject) => {
  		this.http.put(this.normalizeUrl(request['url'], params), params, {
  			headers: this.getHeaders()
  		}).subscribe((x) => {
  			if (x['error']) {
  				reject(x)
  			} else {
  				resolve(x)
  			}
  		})
  	})
  }

  private delete(request: object = {}, params: object = {}): Promise<any> {
  	return new Promise((resolve, reject) => {
  		this.http.delete(this.normalizeUrl(request['url'], params), {
  			headers: this.getHeaders()
  		}).subscribe((x) => {
  			if (x['error']) {
  				reject(x)
  			} else {
  				resolve(x)
  			}
  		})
  	})
  }

  private getHeaders(): HttpHeaders {
  	const headers = {
      'Cache-control': 'no-cache, no-store',
      'Expires': '0',
      'Pragma': 'no-cache',
      'X-Requested-With': 'XMLHttpRequest',
      'X-request-type': 'ajax'
    }
  	
  	return new HttpHeaders(headers)
  }

  private getParams(params: object = {}): HttpParams {
    let body = new HttpParams()
    if (params && Object.keys(params).length > 0) {
      Object.keys(params).forEach((key) => {
        body = body.append(key, params[key])
      })
    }
    return body
  }

  private normalizeUrl(url: string = "", params: object = {}): string {
  	let page_url = url
  	if (params && Object.keys(params).length > 0) {
  		Object.keys(params).forEach((key) => {
  			page_url = page_url.replace(`:${key}`, params[key])
  		})
  	}
  	return page_url 
  }

  private getFakeData(params: any = {}, request: any = {}): any {
    if (!request.fake_data) {
      this.logger.warn('Fake data not found', request)

      return null
    }

    let fake_data = request.fake_data

    if (params && Object.keys(params).length > 0 && request.required_params) {
      Object.keys(params).forEach((key) => {
        if (request.required_params.indexOf(key) != -1) {
          fake_data = fake_data.filter((x) => x[key] == params[key])
        } else {
          this.logger.warn('Parameter not presented in required_params', key)
        }
      })
    }

    return fake_data
  }


}

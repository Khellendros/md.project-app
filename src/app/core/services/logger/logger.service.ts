import { Injectable } from '@angular/core';
import { debug } from 'debug'
import { environment } from '../../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
	private _debug
	private _warn
	private _error
	private _log
	private _info
	private loglevel: string[] = environment.loglevel.split(' ')

  constructor() {
  	this._debug = debug('DEBUG')
  	this._log = debug('LOG')
  	this._info = debug('INFO')
  	this._warn = debug('WARN')
  	this._error = debug('ERROR')

  	this.setBindings()
  }

  public setPrefix(prefix: string) {
  	this._debug = debug(prefix)
  	this._log = debug(prefix)
  	this._info = debug('INFO: ' + prefix)
  	this._warn = debug('WARN: ' + prefix)
  	this._error = debug('ERROR: ' + prefix)
  	this.setBindings()
  }

  private applyLoglevel() {
  	this.loglevel.forEach((x) => {
  		if (this['_' + x]) {
  			this['_' + x].enabled = true
  		}
  	})
  }

  private setBindings() {
  	this.applyLoglevel()
  	this._debug.log = console.debug.bind(console)
  	this._log.log = console.log.bind(console)
  	this._info.log = console.info.bind(console)
  	this._warn.log = console.warn.bind(console)
  	this._error.log = console.error.bind(console)
  }

  get debug() {
  	return this._debug
  }

  get log() {
  	return this._log
  }

  get info() {
  	return this._info
  }

  get warn() {
  	return this._warn
  }

  get error() {
  	return this._error
  }
}

function generateUUID4() {
	let uuid = ''
	for (let i = 0; i < 32; i++) {
		switch (i) {
			case 8:		
			case 20:
				uuid += '-'
				uuid += (Math.random() * 16 | 0).toString(16)
				break;
			case 12:
				uuid += '-'
				uuid += '4'
				break;
			case 16:
				uuid += '-'
				uuid += (Math.random() * 4 | 8).toString(16)
				break;
			default:
				uuid += (Math.random() * 16 | 0).toString(16)
		}
	}
	return uuid
}

export function setUserUUID(api) {
	let user_uuid = localStorage.getItem('user_uuid')
	if (!user_uuid) {
		api.manga.createUser(generateUUID4()).then(response => checkUser(response, api))
	}
	return user_uuid
}

function checkUser(result: any, api: any) {
	if (result['uuid']) {
		localStorage.setItem('user_uuid', result['uuid'])
	} else {
		setUserUUID(api)
	}
}

export function getUserUUID() {
	return localStorage.getItem('user_uuid')
}
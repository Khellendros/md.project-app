import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../core/services/api/api.service'
import { LoggerService } from '../../core/services/logger/logger.service'
import { ActivatedRoute } from '@angular/router'

import { getUserUUID } from '../../core/helpers/uuid4-generator.helper'

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.scss']
})
export class BookmarksComponent implements OnInit {

  public bookmarks: object[] = []
  public display: boolean = true

  constructor(
  	private api: ApiService,
  	private logger: LoggerService,
  	private activated_route: ActivatedRoute,
  ) {
  	logger.setPrefix('Bookmarks')

  	api.manga.getBookmarks(getUserUUID()).then(response => this.getBookmarks(response))
  }

  ngOnInit() {
  }

  public getBookmarks(result: any) {
  	// this.logger.info(result['bookmarks'])
  	
  	this.display = false
  	this.bookmarks = result['bookmarks']
  }

}

import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../core/services/api/api.service'
import { LoggerService } from '../../core/services/logger/logger.service'
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
  	private api: ApiService,
  	private logger: LoggerService,
  	private route: ActivatedRoute,
  ) { 
  	logger.setPrefix('Dashboard')
  	// api.example.get_examples().then(response => this.loadData(response))
  }

  public loadData(response: any) {
  	// this.logger.info(response)
  }

  ngOnInit() {
  }

}

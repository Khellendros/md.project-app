import { DashboardComponent } from './dashboard/dashboard.component'
import { SearchComponent } from './search/search.component'
import { BookmarksComponent } from './bookmarks/bookmarks.component'
import { MangaComponent } from './manga/manga.component'
import { ChapterComponent } from './chapter/chapter.component'

export const Pages = [
	DashboardComponent,
	SearchComponent,
	BookmarksComponent,
	MangaComponent,
	ChapterComponent
]
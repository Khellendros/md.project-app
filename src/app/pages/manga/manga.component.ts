import { Component, OnInit } from '@angular/core';
import { LoggerService } from '../../core/services/logger/logger.service'
import { ApiService } from '../../core/services/api/api.service'
import { Router, ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-manga',
  templateUrl: './manga.component.html',
  styleUrls: ['./manga.component.scss']
})
export class MangaComponent implements OnInit {

  public chapters: object[] = []
  public display: boolean = true

  constructor(
  	private api: ApiService,
  	private logger: LoggerService,
  	private router: Router,
  	private activated_route: ActivatedRoute,
  ) {
  	logger.setPrefix('Manga')

  	let manga_id = activated_route.snapshot.params.id

  	api.manga.showManga(manga_id).then(response => this.showManga(response))
  }

  ngOnInit() {
  }

  public showManga(result: any) {
  	this.display = false
  	
  	this.chapters = result['chapters']
  }

}

import { Component, OnInit } from '@angular/core';
import { LoggerService } from '../../core/services/logger/logger.service'
import { ApiService } from '../../core/services/api/api.service'
import { Router } from '@angular/router'

import { getUserUUID } from '../../core/helpers/uuid4-generator.helper'

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  public mangas: object[] = []

  public display: boolean = false

  constructor(
  	private logger: LoggerService,
    private api: ApiService,
    private router: Router,
  ) {
  	logger.setPrefix('Search')
  }

  ngOnInit() {
  }

  public onSearch(searchText: string) {
    this.display = true
  	this.api.manga.search(searchText).then(response => this.searchResults(response))
  }

  public searchResults(result: any) {
    // this.logger.info(result['results'])

    this.display = false
    if (typeof result['results'] != 'string') {
      this.mangas = result['results']
    } else {
      this.mangas = []
    }
  }

  public onAddManga(manga: any) {
    this.display = true

    this.api.manga.createBookmark(manga.link, manga.title, manga.cover, getUserUUID() ).then(response => this.bookmarkResults(response))
  }

  public bookmarkResults(result: any) {
    if (result['manga_id']) {
      this.router.navigate(['/mangas/'+result['manga_id']])
    } else {
      this.display = true
    }
  }

}

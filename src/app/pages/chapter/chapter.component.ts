import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../core/services/api/api.service'
import { LoggerService } from '../../core/services/logger/logger.service'
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-chapter',
  templateUrl: './chapter.component.html',
  styleUrls: ['./chapter.component.scss']
})
export class ChapterComponent implements OnInit {

  public image: string = ''
  public chapters_select: object[] = []
  public images_select: object[] = []

  public display: boolean = true
  public original_images: object[] = []
  public current_slide: number = 0;

  public current_chapter_id: number;
  public current_chapter_position: number = 0;

  public show_prev: boolean = false
  public show_next: boolean = true


  constructor(
  	private api: ApiService,
  	private logger: LoggerService,
  	private activated_route: ActivatedRoute,
    private router: Router,
  ) {
  	logger.setPrefix('Chapter')

    activated_route.params.subscribe(val => {
      this.display = true
      this.current_chapter_id = activated_route.snapshot.params.id
      this.current_slide = 0
      api.manga.showChapter(this.current_chapter_id).then(response => this.showChapter(response))
    })

  }

  public ngOnInit() {
    this.display = true
  }

  public showChapter(result: any) {
    this.original_images = result['images']

    this.chapters_select = result['chapters'].map((value, index, array) => {
      return {
        value: value['position'],
        position: index
      }
    })
    this.current_chapter_position = this.chapters_select.find((el) => {
      return el['value'] == this.current_chapter_id
    })['position']
    this.logger.info(this.current_chapter_position)
    this.images_select = result['images'].map((value, index, array) => {
      return {
        value: value['position'],
        position: index
      }
    })

    this.showSlide()
  }

  private showSlide() {
      this.display = true
      this.api.manga.getImage64(this.original_images[this.current_slide]['link']).then(response => this.getImage(response))
  }

  private getImage(result: any) {
    this.image = result['image64']
    this.display = false
  }

  public nextSlide() {
    let new_position = this.current_slide + 1
    if (new_position <= this.original_images.length) {
      this.changeSlide(new_position)
    }
  }

  public prevSlide() {
    let new_position = this.current_slide - 1
    if (new_position >= 0) {
      this.changeSlide(new_position)
    }
  }

  public changeSlide(value: number) {
    this.current_slide = value

    switch (value) {
      case this.original_images.length:
        this.show_next = false
        this.show_prev = true
        break;
      case 0:
        this.show_next = true
        this.show_prev = false
        break;
      default:
        this.show_next = true
        this.show_prev = true
        break;
    }
    this.showSlide()
  }

  public changeChapter(value: number) {
    this.router.navigate(['chapters', value])
  }


}

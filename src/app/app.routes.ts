import { RouterModule, Routes } from '@angular/router';

import { MangasRoutes } from './routes/manga/manga.routes'

export const AppRoutes: Routes = [
	...MangasRoutes,
]